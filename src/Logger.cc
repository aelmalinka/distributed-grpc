/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Exception.hh"

using namespace std;
using namespace Coeus;

namespace DistributedGrpc {
	Log::Source Log("Distributed GRPC"s);
}

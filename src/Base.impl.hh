/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_BASE_IMPL
#	define DISTRIBUTEDGRPC_BASE_IMPL

#	include "Base.hh"

	namespace DistributedGrpc
	{
		template<typename Service>
		Base<Service>::Base(std::shared_ptr<WorkQueue> const &queue) :
			_queue(queue),
			_service()
		{}

		template<typename Service> Base<Service>::~Base() = default;

		template<typename Service>
		template<typename In, typename Out>
		auto Base<Service>::write(
			Base<Service>::WriteF<In, Out> q,
			std::function<void(In const &, Out *, void *)> const &f
		) -> void {
			_queue->work([q, f, this](auto &i) {
				auto ctx = grpc::ServerContext();
				auto in = std::make_shared<In>();
				auto out = std::make_shared<Out>(&ctx);

				std::invoke(q, _service,
					&ctx,
					in.get(), out.get(),
					&_queue->queue(), &_queue->queue(),
					reinterpret_cast<void *>(0)
				);

				return [f, i, in, out]() {
					f(*in, out.get(), reinterpret_cast<void *>(i));
				};
			});
		}

		template<typename Service>
		template<typename In>
		auto Base<Service>::read(
			Base<Service>::ReadF<In> const &q,
			std::function<void(In *, void *)> const &f
		) -> void {
			_queue->work([q, f, this](auto &i) {
				auto ctx = grpc::ServerContext();
				auto in = std::make_shared<In>(&ctx);

				std::invoke(q, &_service,
					&ctx,
					in.get(),
					&_queue->queue(), &_queue->queue(),
					reinterpret_cast<void *>(i)
				);

				return [f, i, in]() {
					f(in.get(), reinterpret_cast<void *>(i));
				};
			});
		}
	}

#endif

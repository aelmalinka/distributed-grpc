/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Egress.hh"

using namespace DistributedGrpc;
using namespace std;
using namespace distributed;
using namespace google::protobuf;
using namespace grpc;

Egress::Egress(shared_ptr<WorkQueue> const &queue) :
	Base<distributed::egress::Egress::AsyncService>(queue)
{
	write<Backend, ServerAsyncResponseWriter<Empty>>(
		&distributed::egress::Egress::AsyncService::RequestAddBackend,
		bind_front(&Egress::add_backend, this)
	);

	write<Backend, ServerAsyncResponseWriter<Empty>>(
		&distributed::egress::Egress::AsyncService::RequestRemoveBackend,
		bind_front(&Egress::remove_backend, this)
	);

	write<Request, ServerAsyncResponseWriter<Any>>(
		&distributed::egress::Egress::AsyncService::RequestUnaryUnary,
		bind_front(&Egress::unary_unary, this)
	);

	write<Request, ServerAsyncWriter<Any>>(
		&distributed::egress::Egress::AsyncService::RequestUnaryStream,
		bind_front(&Egress::unary_stream, this)
	);

	read<ServerAsyncReader<Any, Request>>(
		&distributed::egress::Egress::AsyncService::RequestStreamUnary,
		bind_front(&Egress::stream_unary, this)
	);

	read<ServerAsyncReaderWriter<Any, Request>>(
		&distributed::egress::Egress::AsyncService::RequestStreamStream,
		bind_front(&Egress::stream_stream, this)
	);
}

Egress::~Egress() = default;

auto Egress::add_backend(Backend const &, ServerAsyncResponseWriter<Empty> *, void *) -> void {
	COEUS_THROW(Exception("To Do"));
}

auto Egress::remove_backend(Backend const &, ServerAsyncResponseWriter<Empty> *, void *) -> void {
	COEUS_THROW(Exception("To Do"));
}

auto Egress::unary_unary(Request const &, ServerAsyncResponseWriter<Any> *, void *) -> void {
	COEUS_THROW(Exception("To Do"));
}

auto Egress::unary_stream(Request const &, ServerAsyncWriter<Any> *, void *) -> void {
	COEUS_THROW(Exception("To Do"));
}

auto Egress::stream_unary(ServerAsyncReader<Any, Request> *, void *) -> void {
	COEUS_THROW(Exception("To Do"));
}

auto Egress::stream_stream(ServerAsyncReaderWriter<Any, Request> *, void *) -> void {
	COEUS_THROW(Exception("To Do"));
}

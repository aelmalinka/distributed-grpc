/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_APPLICATION_INC
#	define DISTRIBUTEDGRPC_APPLICATION_INC

#	include "Exception.hh"
#	include <Coeus/Application.hh>
#	include <optional>

//#	include "Broker.hh"
#	include "Ingress.hh"
#	include "Egress.hh"
//#	include "Worker.hh"

	namespace DistributedGrpc
	{
		class Application :
			public ::Coeus::Application
		{
			public:
				Application(int const ArgC, char const *ArgV[]);
				virtual ~Application();
				virtual auto operator () () -> void override;
			protected:
				virtual auto run() -> void;
			private:
				Coeus::options_description _opts;
				std::size_t _thread_count;
				std::optional<std::string> _ingress;
				std::optional<std::string> _egress;
		};
	}

#endif

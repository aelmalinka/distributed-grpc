/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_MONOTONIC_IMPL
#	define DISTRIBUTEDGRPC_MONOTONIC_IMPL

	namespace DistributedGrpc
	{
		template<std::convertible_to<Monotonic::duration> U>
		Monotonic::Monotonic(boost::asio::io_context &io, decltype(_cb) const &cb, U const &tick_rate) :
			_cb(cb),
			_timer(io),
			_tick_rate(tick_rate)
		{
			_timer.expires_after(std::chrono::seconds(0));
			_timer.async_wait(bind(&Monotonic::tick, this, std::placeholders::_1));
		}

		template<std::convertible_to<Monotonic::duration> From>
		auto Monotonic::setTickRate(From &&from) -> void {
			_tick_rate = std::forward<From>(from);
		}
	}

#endif

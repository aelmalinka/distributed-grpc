/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "WorkQueue.hh"
#include <cassert>

using namespace DistributedGrpc;
using namespace std;
using namespace std::chrono;

using Coeus::Severity;

WorkQueue::~WorkQueue() = default;

auto WorkQueue::cancel() -> void {
	_ticker.cancel();
	_queue->Shutdown();
}

auto WorkQueue::work(function<function<void()>(size_t const &)> &&f) -> size_t {
	auto i = _work.size();

	_work.emplace_back(f);
	auto n = _work.back().requeue(i);

	swap(_work.back(), n);
	assert(_work.size() == i + 1);

	return i;
}

auto WorkQueue::queue() -> decltype(*_queue) {
	return *_queue;
}

auto WorkQueue::tick() -> void {
	while(true) {
		void *tag = nullptr;
		auto ok = false;

		COEUS_LOG(Log, Severity::Debug) << "Polling for completed events";
		// short deadline to free up for next tick call
		_queue->AsyncNext(&tag, &ok, system_clock::now() + milliseconds(10));

		COEUS_LOG(Log, Severity::Debug) << "Got event " << ok << " (" << tag << ")";
		// wait till next tick
		if (!ok) break;

		auto pos = reinterpret_cast<size_t>(tag);
		auto next = _work[pos].requeue(pos);

		swap(_work[pos], next);

		next();
	}
}

WorkQueue::Work::Work(function<function<void()>(size_t const &)> const &create) :
	_create(create),
	_work()
{}

WorkQueue::Work::Work(function<function<void()>(size_t const &)> const &create, function<void()> const &work) :
	_create(create),
	_work(work)
{}

auto WorkQueue::Work::operator () () -> void {
	if(!_work)
		COEUS_THROW(Exception("Invalid Work (null _work)"));

	_work();
}

auto WorkQueue::Work::requeue(size_t const &i) -> Work {
	return Work(_create, _create(i));
}

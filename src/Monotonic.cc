/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Monotonic.hh"

using namespace DistributedGrpc;
using namespace std;

Monotonic::~Monotonic() = default;

auto Monotonic::cancel() -> void {
	_timer.cancel();
}

auto Monotonic::tick(error_code const &ec) -> void {
	if(!ec) {
		_timer.expires_after(_tick_rate);
		_timer.async_wait(bind(&Monotonic::tick, this, placeholders::_1));

		_cb();
	}
}

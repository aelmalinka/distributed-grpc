/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_WORKQUEUE_INC
#	define DISTRIBUTEDGRPC_WORKQUEUE_INC

#	include "Monotonic.hh"
#	include "Exception.hh"
#	include <grpcpp/grpcpp.h>

	namespace DistributedGrpc
	{
		// 2022-05-09 AMR TODO: split tempaltes off to impl file
		// 2022-05-09 AMR TODO: who owns the server
		class WorkQueue {
			public:
				class Work;
			private:
				std::unique_ptr<grpc::ServerCompletionQueue> _queue;
				std::vector<Work> _work;
				Monotonic _ticker;
			public:
				template<std::convertible_to<Monotonic::duration> U = std::chrono::milliseconds>
				WorkQueue(
					boost::asio::io_context &io,
					std::unique_ptr<grpc::ServerCompletionQueue> &&queue,
					U const &tick_rate = std::chrono::milliseconds(100)
				);
				virtual ~WorkQueue();
				virtual auto cancel() -> void;
				virtual auto work(std::function<std::function<void()>(std::size_t const &)> &&) -> std::size_t;
				virtual auto queue() -> decltype(*_queue);
			protected:
				virtual auto tick() -> void;
			public:
				class Work {
					private:
						std::function<std::function<void()>(std::size_t const &)> _create;
						std::function<void()> _work;
					public:
						explicit Work(std::function<std::function<void()>(std::size_t const &)> const &create);
						auto operator () () -> void;
						auto requeue(std::size_t const &) -> Work;
					protected:
						Work(
							std::function<std::function<void()>(std::size_t const &)> const &create,
							std::function<void()> const &
						);
				};
		};
	}

#	include "WorkQueue.impl.hh"

#endif

/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <cstdlib>
#include <iostream>
#include "Application.hh"

using namespace std;
using namespace DistributedGrpc;

int main(int argc, char const *argv[]) {
	auto app = Application(argc, argv);

	try {
		app();

		return EXIT_SUCCESS;
	} catch(exception &e) {
		cerr << e << endl;
	}

	return EXIT_FAILURE;
}

/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_EGRESS_INC
#	define DISTRIBUTEDGRPC_EGRESS_INC

#	include "Base.hh"
#	include "egress.grpc.pb.h"

	namespace DistributedGrpc
	{
		class Egress :
			public Base<distributed::egress::Egress::AsyncService>
		{
			public:
				explicit Egress(std::shared_ptr<WorkQueue> const &);
				virtual ~Egress();
			public:
				virtual auto add_backend(
					distributed::Backend const &,
					grpc::ServerAsyncResponseWriter<google::protobuf::Empty> *,
					void *
				) -> void;
				virtual auto remove_backend(
					distributed::Backend const &,
					grpc::ServerAsyncResponseWriter<google::protobuf::Empty> *,
					void *
				) -> void;

				virtual auto unary_unary(
					distributed::Request const &,
					grpc::ServerAsyncResponseWriter<google::protobuf::Any> *,
					void *tag
				) -> void;
				virtual auto unary_stream(
					distributed::Request const &,
					grpc::ServerAsyncWriter<google::protobuf::Any> *,
					void *
				) -> void;

				virtual auto stream_unary(
					grpc::ServerAsyncReader<google::protobuf::Any, distributed::Request> *,
					void *
				) -> void;
				virtual auto stream_stream(
					grpc::ServerAsyncReaderWriter<google::protobuf::Any, distributed::Request> *,
					void *
				) -> void;
		};
	}

#endif

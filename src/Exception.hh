/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_EXCEPTION_INC
#	define DISTRIBUTEDGRPC_EXCEPTION_INC

#	include <Coeus/Exception.hh>
#	include <Coeus/Log.hh>

	namespace DistributedGrpc
	{
		using ::Coeus::Log::Severity;

		COEUS_EXCEPTION(Exception, "Distributed GRPC Exception", ::Coeus::Exception);

		extern ::Coeus::Log::Source Log;
	}

#endif

/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_MONOTONIC_INC
#	define DISTRIBUTEDGRPC_MONOTONIC_INC

#	include <chrono>
#	include <concepts>
#	include <optional>
#	include <utility>
// 2022-05-09 AMR TODO: can be updated to Networking TS?
#	include <boost/asio/io_context.hpp>
#	include <boost/asio/steady_timer.hpp>

	namespace DistributedGrpc
	{
		// 2022-05-09 AMR TODO: promote to Coeus?
		// 2022-05-09 AMR TODO: split tempaltes off to impl file
		class Monotonic {
			public:
				using duration = std::chrono::steady_clock::duration;
			private:
				std::function<void()> _cb;
				boost::asio::steady_timer _timer;
				duration _tick_rate;
			public:
				// 2022-05-07 AMR TODO: sane defaults?
				template<std::convertible_to<duration> U = std::chrono::seconds>
				Monotonic(boost::asio::io_context &io, decltype(_cb) const &cb, U const &tick_rate = std::chrono::seconds(1));
				virtual ~Monotonic();
				virtual auto cancel() -> void;
				template<std::convertible_to<decltype(_tick_rate)> From>
				auto setTickRate(From &&from) -> void;
			protected:
				virtual auto tick(std::error_code const &ec) -> void;
		};
	}

#	include "Monotonic.impl.hh"

#endif

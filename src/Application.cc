/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

#include "Application.hh"

#include "Monotonic.hh"
#include <iostream>
#include <thread>
#include <vector>

using namespace DistributedGrpc;
using namespace std;
using namespace std::chrono;
using namespace boost::asio;

using Coeus::value;

const auto THREADS = thread::hardware_concurrency();

Application::Application(
	int const ArgC,
	char const *ArgV[]
) :
	::Coeus::Application(ArgC, ArgV)
{
	_opts.add_options()
		("thread-count,t", value(&_thread_count)->default_value(THREADS), "The number of threads")
	;

	command_line().add(_opts);
	config_file().add(_opts);
}

Application::~Application() = default;

// 2022-05-08 AMR NOTE: in main
auto Application::operator () () -> void {
	::Coeus::Application::operator () ();

	// 2022-05-08 AMR TODO: construct requested listeners

	auto threads = vector<jthread>(_thread_count);

	COEUS_LOG(Log, Severity::Info) << "Spawning " << _thread_count << " threads";
	for(auto i = 0u; i < _thread_count; i++) threads.emplace_back(&Application::run, this);
}

// 2022-05-08 AMR NOTE: threads
auto Application::run() -> void {
	auto io = io_context();
	//auto queue = WorkQueue(io);

	// 2022-05-12 AMR TODO: construct requested services

	io.run();
}

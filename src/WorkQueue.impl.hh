/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_WORKQUEUE_IMPL
#	define DISTRIBUTEDGRPC_WORKQUEUE_IMPL

	namespace DistributedGrpc
	{
		template<std::convertible_to<Monotonic::duration> U>
		WorkQueue::WorkQueue(
			boost::asio::io_context &io,
			std::unique_ptr<grpc::ServerCompletionQueue> &&queue,
			U const &tick_rate
		) :
			_queue(std::move(queue)),
			_ticker(io, std::bind(&WorkQueue::tick, this), tick_rate)
		{}
	}

#endif

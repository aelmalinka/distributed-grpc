/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_BASE_INC
#	define DISTRIBUTEDGRPC_BASE_INC

#	include "WorkQueue.hh"
#	include <grpcpp/grpcpp.h>

	namespace DistributedGrpc
	{
		// 2022-05-08 AMR TODO: concept for grpc?
		template<typename Service>
		class Base {
			public:
				template<typename In, typename Out>
				using WriteF = void (Service::*)(
					grpc::ServerContext *,
					In *, Out *,
					grpc::CompletionQueue *, grpc::ServerCompletionQueue *,
					void *
				);

				template<typename In>
				using ReadF = void (Service::*)(
					grpc::ServerContext *,
					In *,
					grpc::CompletionQueue *, grpc::ServerCompletionQueue *,
					void *
				);
			private:
				std::shared_ptr<WorkQueue> _queue;
				Service _service;
			public:
				explicit Base(std::shared_ptr<WorkQueue> const &);
				virtual ~Base();
			protected:
				// 2022-05-10 AMR TODO: concepts?
				template<typename In, typename Out>
				auto write(WriteF<In, Out>, std::function<void(In const &, Out *, void *)> const &) -> void;
				template<typename In>
				auto read(ReadF<In> const &, std::function<void(In *, void *)> const &) -> void;
		};
	}

#	include "Base.impl.hh"

#endif

/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Ingress.hh"

using namespace DistributedGrpc;
using namespace std;

Ingress::~Ingress() = default;

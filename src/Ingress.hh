/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined DISTRIBUTEDGRPC_INGRESS_INC
#	define DISTRIBUTEDGRPC_INGRESS_INC

#	include "Base.hh"
#	include "ingress.grpc.pb.h"

	namespace DistributedGrpc
	{
		class Ingress :
			public Base<distributed::ingress::Ingress::AsyncService>
		{
			public:
				virtual ~Ingress();
		};
	}

#endif

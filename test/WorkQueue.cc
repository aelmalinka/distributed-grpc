/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/WorkQueue.hh"

using namespace testing;
using namespace std;
using namespace boost::asio;
using namespace DistributedGrpc;
using namespace grpc;

namespace {
	TEST(WorkQueueTests, DISABLED_Simple) {
		// 2022-05-09 AMR TODO: have to trigger grpc::ServerCompletionQueue in order to actually test anything
	}
	TEST(WorkQueueTests, Cancels) {
		auto worked = true;
		auto builder = ServerBuilder();
		auto io = io_context();
		auto queue = WorkQueue(io, builder.AddCompletionQueue());

		queue.cancel();

		// 2022-05-09 AMR NOTE: the create callback *will* be called
		queue.work([&worked](auto &) {
			return [&worked]() {
				worked = false;
			};
		});

		io.run();

		EXPECT_TRUE(worked);
	}
}

/*	Copyright 2022 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Monotonic.hh"

using namespace testing;
using namespace std;
using namespace std::chrono;
using namespace boost::asio;
using namespace DistributedGrpc;

namespace {
	TEST(MonotonicTests, Ticks) {
		auto x = 0u;
		auto io = io_context();
		auto timer = Monotonic(io, [&x](){ ++x; }, milliseconds(5));

		io.run_for(milliseconds(12));

		EXPECT_EQ(x, 3u);
	}
	TEST(MonotonicTests, Cancelable) {
		auto x = 0u;
		auto io = io_context();
		auto timer = Monotonic(io, [&x]() { ++x; }, milliseconds(2));

		io.run_one();
		timer.cancel();

		io.run();

		EXPECT_EQ(x, 1u);
	}
	TEST(MonotonicTests, DestroyedIsCanceled) {
		auto x = 0u;
		auto io = io_context();

		{
			auto timer = Monotonic(io, [&x]() { ++x; }, milliseconds(1));
		}

		io.run_for(milliseconds(3));
		EXPECT_EQ(x, 0u);
	}
	TEST(MonotonicTests, WorksWithBind) {
		auto x = 0u;
		auto io = io_context();
		auto f = [](auto &x) { ++x; };
		auto timer = Monotonic(io, bind(f, ref(x)), milliseconds(5));

		io.run_for(milliseconds(12));

		EXPECT_EQ(x, 3u);
	}
}
